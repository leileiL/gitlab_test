package com.lsm.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;


@Aspect
@Component  //注解扫描
public class LogAspect {

    //拿到日志记录器
    private final Logger logger= LoggerFactory.getLogger(this.getClass());

    @Pointcut("execution(* com.lsm.web.*.*(..))")  //（任意返回值类型 包名.任意类.任意方法（任意参数个数）
    public void log(){}  //切入点表达式，提供一个唯一标识

    @Before("log()")
    public void deBefore(JoinPoint joinPoint){
        ServletRequestAttributes attributes= (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        ServletRequest request=attributes.getRequest();
        String url= ((HttpServletRequest) request).getRequestURL().toString();
        String ip=request.getRemoteAddr();
        String classMethod=joinPoint.getSignature().getDeclaringTypeName()+"."+joinPoint.getSignature().getName();
        Object[] args=joinPoint.getArgs();
        RequestLog requestLog=new RequestLog(url,ip,classMethod,args);
        logger.info("Request: {}",requestLog);
        //logger.info("-------doBefore--------");

    }

    @After("log()")
    public void doAfter(){
        //logger.info("-------doAfter--------");
    }

    @AfterReturning(returning = "result",pointcut = "log()")
    public void doAfterReturn(Object result){
        logger.info("Result: {}",result);

    }

    //使用类封装起来
    private class RequestLog{
        private String url;
        private String ip;
        private String classMethod;  //调用的哪个方法
        private Object[] args;  //参数获取

        public RequestLog(String url, String ip, String classMethod, Object[] args) {
            this.url = url;
            this.ip = ip;
            this.classMethod = classMethod;
            this.args = args;
        }

        @Override
        public String toString() {
            return "RequestLog{" +
                    "url='" + url + '\'' +
                    ", ip='" + ip + '\'' +
                    ", classMethod='" + classMethod + '\'' +
                    ", args=" + Arrays.toString(args) +
                    '}';
        }
    }
}
