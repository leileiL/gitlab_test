package com.lsm.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/*自定义的错误拦截*/
@ControllerAdvice   //会拦截所有标注了controller的异常
public class ControllerExceptionHandler {

    //获取日志记录器对象
    private final Logger logger=LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(Exception.class)  //标识这个方法是进行异常处理的
    public ModelAndView exceptionHandler(HttpServletRequest request,Exception e) throws Exception {
        logger.error("Request URL: {}, Exception: {}",request.getRequestURL(),e);

        if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class)!=null){
            throw e;  //给出异常状态码的判断，如果不存在则交由spingboot处理，返回到404页面
        }

        ModelAndView mv=new ModelAndView();
        mv.addObject("url",request.getRequestURL());
        mv.addObject("exception",e);
        mv.setViewName("error/error");  //返回error页面
        return mv;
    }


}
