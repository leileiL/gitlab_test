package com.lsm.service;

import com.lsm.dao.UserRepository;
import com.lsm.po.User;
import com.lsm.util.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;  //使用jpa，在数据库中查看是否有匹配的用户user

    @Override
    public User checkUser(String username, String password) {

        User user=userRepository.findByUsernameAndPassword(username, MD5Utils.code(password));
        return user;

    }
}

