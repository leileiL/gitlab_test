package com.lsm.service;

import com.lsm.po.Blog;
import com.lsm.po.Tag;
import com.lsm.vo.BlogQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface BlogService {

    //根据id查询
    Blog getBlog(Long id);

    //为了将markdown转换的方法
    Blog getAndConvert(Long id);

   // Blog getAndConvert(Long id);

    //组合查询
    Page<Blog> listBlog(Pageable pageable, BlogQuery blog);

    Page<Blog> listBlog(Pageable pageable);

    Page<Blog> listBlog(String query,Pageable pageable);  //用以index页面的search

    Page<Blog> listBlog(Long tagId,Pageable pageable);  //用于blog和tag联合查询

    List<Blog> listRecommendBlogTop(Integer size); //前端index页面的最新推荐

    Map<String,List<Blog>> archiveBlog(); //用于归档archive页面

    Long countBlog();

    //新增
    Blog saveBlog(Blog blog);

    //修改：先根据id查询，然后再赋值
    Blog updateBlog(Long id,Blog blog);

    //删除
    void deleteBlog(Long id);

}
