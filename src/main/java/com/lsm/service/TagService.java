package com.lsm.service;

import com.lsm.po.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;


public interface TagService {

    Tag saveTag(Tag tag);

    Tag getTag(Long id);

    Tag getTagByName(String name);

    Page<Tag> listTag(Pageable pageable);

    List<Tag> listTag();  //获取到所有的tag

    List<Tag> listTagTop(Integer size);  //index 页面的tag

    List<Tag> listTag(String ids);  //多个id

    Tag updateTag(Long id,Tag tag);

    void deleteTag(Long id);
}
