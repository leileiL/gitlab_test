package com.lsm.service;

import com.lsm.po.User;

public interface UserService {

    //检查用户登录
    User checkUser(String username,String password);
}
