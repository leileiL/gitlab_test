package com.lsm.service;

import com.lsm.po.Type;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TypeService {

    Type saveType(Type type);  //新增后的保存

    Type getType(Long id); //新增后的查询

    Type getTypeByName(String name);

    Page<Type> listType(Pageable pageable);  //分页查询

    List<Type> listType();

    List<Type> listTypeTop(Integer size);

    Type updateType(Long id,Type type);//修改：根据id先查询，然后再修改

    void  deleteType(Long id); //根据id删除
}
