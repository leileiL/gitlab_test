package com.lsm.vo;

/*将blog页面组合查询的属性封装在一起*/
/*将blog页面组合查询的属性封装在一起*/
public class BlogQuery {

    private String title;
    private Long typeId;
    private boolean recommend;

    public BlogQuery(){

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public boolean isRecommend() {   //一定要为bolean类型，不然isRecommend（）为空指针
        return recommend;
    }

    public void setRecommend(boolean recommend) {
        this.recommend = recommend;
    }
}
